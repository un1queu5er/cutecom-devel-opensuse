FROM	opensuse/tumbleweed
LABEL	maintainer="Meinhard Ritscher <cyc1ingsir@gmail.com>" \
 version="0.6" \
 git-url="https://gitlab.com/cutecom/cutecom-devel-opensuse"


RUN zypper --non-interactive addrepo https://download.opensuse.org/repositories/devel:/gcc/openSUSE_Tumbleweed/devel:gcc.repo | echo 'a' && \
    zypper --non-interactive addrepo https://download.opensuse.org/repositories/KDE:Qt6/openSUSE_Tumbleweed/KDE:Qt6.repo | echo 'a' && \
    zypper ref && \ 
    zypper --non-interactive in \
    patterns-kde-devel_qt6 \
    qt6-core-devel \
    qt6-widgets-devel \
    qt6-network-devel \
    qt6-serialport-devel \
    git-core \ 
    python \ 
    clang15

